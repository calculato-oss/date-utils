package br.com.calculato.dateutils

import java.time.Duration
import java.time.Period

/**
 * Helper method to converts [this] in duration as minutes unites
 */
fun Short.toMinutesOfDuration() = Duration.ofMinutes(this.toLong())

operator fun Duration.div(i: Long): Duration = this.dividedBy(i)

operator fun Duration.times(i: Long): Duration = this.multipliedBy(i)

fun Int.weeks() = Period.ofWeeks(this)
fun Int.days() = Period.ofDays(this)
fun Int.months() = Period.ofMonths(this)
fun Int.years() = Period.ofYears(this)