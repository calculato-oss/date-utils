[![](https://jitpack.io/v/com.gitlab.calculato-oss/date-utils.svg)](https://jitpack.io/#com.gitlab.calculato-oss/date-utils)

![Squad calculato](https://img.shields.io/badge/squad-calculato-blue)
![dependency version](https://img.shields.io/badge/Kotlin_JVM-1.3.72-orange)
![dependency version](https://img.shields.io/badge/Ktor-1.3.2-orange)
![dependency version](https://img.shields.io/badge/Exposed-0.25.1-orange)
# Ktor Date Utils

Biblioteca Kotlin com complementos para auxiliar no uso da biblioteca de horas do kotlin e java8.

## O que essa biblioteca faz?

* Funções que retornam diferentes tratamentos para as datas e horas
* Funções que tratam `períodos` no formato (AAAAMM)

## Como usar

Passo 1: Adicione em repositories do arquivo build.gradle o jitpack.io:
```
repositories {
    maven { url 'https://jitpack.io' }
}
```
Passo 2: Adicione em dependencies
```
implementation "com.gitlab.calculato-oss:date-utils:$version"
```

[Usar em outras ferramentas de build](https://jitpack.io/v/com.gitlab.calculato-oss/date-utils)